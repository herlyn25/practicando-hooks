import {Menu} from "./components/Menu"
import {Usuario} from './components/Usuario'
import {Section} from "./components/Section"
import { Comentario } from "./components/Comentario"
import {useEffect, useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css'
import { Fotos } from "./components/Fotos"

const URL_USER="https://jsonplaceholder.typicode.com/users"
const URL_FOTOS="https://jsonplaceholder.typicode.com/photos"
const URL_COMENTARIOS="https://jsonplaceholder.typicode.com/comments"

function App() {
  let [usuarios, setUsuarios] = useState([])
  let [fotos, setFotos] = useState([])
  let [comentarios, setComentarios] = useState([])
    useEffect(() =>{
    fetch(URL_USER)
    .then(response=> response.json())
    .then((datos) => setUsuarios(datos))
    },[])
    useEffect(() =>{
      fetch(URL_FOTOS)
      .then(response=> response.json())
      .then((datos) => setFotos(datos))
    },[])
    useEffect(() =>{
      fetch(URL_COMENTARIOS)
      .then(response=> response.json())
      .then((datos) => setComentarios(datos))
      },[])
  
  return (
    <div className="container">      
      <Menu/>
      <Section titulo="Usuarios">
        <div className="container">
          <div className="row">
            {usuarios.slice(0,9).map((usuario)=>{
            return (
              <Usuario 
              key={usuario.username} 
              nombre={usuario.name} 
              apodo={usuario.username} 
              correo={usuario.email} 
              />
            )     
            })}
          </div>
        </div>        
      </Section>
      <Section titulo="Fotos">
         <div className="container">
           <div className="row">
           {fotos.slice(0,8).map((foto)=>{
            return (
              <Fotos key={foto.id} titulo={foto.title} urlImagen={foto.thumbnailUrl} />
            )
          })}
           </div>
         </div>
      </Section>
      <Section titulo="Comentarios">
          <div className="container">
              <div className="row">
              {comentarios ? comentarios.slice(0,12).map((comentario)=>{
                return (
                  <Comentario key={comentario.id} nombre={comentario.name} contenido={comentario.body} />
                )
              }): <h1>Cargando los comentarios</h1> }
              </div>
          </div>
      </Section>
    </div>
  )
}

export default App
