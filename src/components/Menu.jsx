export function Menu(){
    return (
        <ul className="nav">
            <li className="nav-item">
                <button className="nav-link active">
                    Usuarios
                </button>
            </li>
            <li className="nav-item">
                <button className="nav-link active">
                    Fotos
                </button>
            </li>
            <li className="nav-item">
                <button className="nav-link active">
                    Comentarios
                </button>
            </li>
        </ul>
    );
}