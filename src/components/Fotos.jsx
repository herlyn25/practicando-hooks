export function Fotos({titulo,urlImagen}){
    return (
        <div className="card" style={{width:'18rem'}}>
            <img src={urlImagen} className="card-img-top" alt="titulo"/>
            <div className="card-body">
                <p className="card-text">
                    {titulo}
                </p>
            </div>
        </div>
    )
}