export function Comentario({nombre,contenido}){
    return (
    <div className='card' style={{ width: '18rem' }}>
      <div className='card-body'>
        <h5 className='card-title'>{nombre}</h5>
        <p className='card-text'>{contenido}</p>
      </div>
    </div>
    )
}