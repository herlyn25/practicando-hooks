export function Section({titulo,children}){
    return (
        <div className="py-2">
            <section>
                <h2>{titulo}</h2>
                {children}                   
            </section>           
        </div>
    )
}