export function Usuario({nombre, apodo, correo}){
    return (
      <div className='col-3 m-3'>
        <div className="card" style={{width: '18rem'}}>
            <ul className="list-group list-group-flush">
                <li className="list-group-item"> {nombre}</li>
                <li className="list-group-item"> {apodo}</li>
                <li className="list-group-item"> {correo}</li>
            </ul>
        </div>
      </div>
    )
}